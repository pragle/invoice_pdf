FROM maven:3.5.3-alpine
MAINTAINER michal@vane.pl
WORKDIR /
RUN apk --no-cache add git curl unzip
# clone project
COPY . /invoice_pdf
WORKDIR /invoice_pdf
# get font and place it in fonts/open-sans
RUN curl -X GET https://fonts.google.com/download?family=Open%20Sans > font.zip
RUN mkdir -p fonts/open-sans
RUN unzip font.zip -d fonts/open-sans
RUN mvn install
EXPOSE 8888
CMD mvn exec:java