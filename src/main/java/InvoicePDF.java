
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.apache.pdfbox.pdmodel.PDDocument;
import pl.vane.invoice.controller.DocumentController;
import pl.vane.invoice.factory.PDFFactory;
import pl.vane.invoice.model.Invoice;
import pl.vane.invoice.model.pdf.DocumentColumns;

import java.io.*;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: michal@vane.pl
 * Date: 28.02.15
 * Time: 22:14
 * To change this template use File | Settings | File Templates.
 */
public class InvoicePDF {

    public static void main(String[] args) {
        try {
            new InvoicePDF().start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    final ObjectMapper mapper = new ObjectMapper();
    final String FONT_PATH = "fonts/open-sans/OpenSans-Regular.ttf";

    DocumentColumns documentColumns;

    private void start() throws Exception {
        initPDF();
        HttpServer http = HttpServer.create(new InetSocketAddress(8888), 0);
        http.createContext("/api", new HttpHandler() {
            @Override
            public void handle(HttpExchange req) throws IOException {
                System.out.println("REQ -> "+req.getRequestMethod());
                if(req.getRequestMethod().equals("POST")) {
                    String data = null;
                    try {
                        data = readBodyAsString(req);
                        Invoice invoice = mapper.readValue(data, Invoice.class);
                        System.out.println(invoice);
                        req.sendResponseHeaders(200, 0L);
                        OutputStream resp = req.getResponseBody();
                        createPDF(invoice, resp);
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    System.out.println(data);
                }  else {
                    req.sendResponseHeaders(200, 0L);
                    PrintWriter p = new PrintWriter(req.getResponseBody());
                    p.println("Use POST");
                    p.close();
                }

            }
        });
        http.setExecutor(null);
        http.start();
    }

    private String readBodyAsString(HttpExchange req) throws Exception {
        InputStreamReader isr =  new InputStreamReader(req.getRequestBody(),"utf-8");
        BufferedReader br = new BufferedReader(isr);

// From now on, the right way of moving from bytes to utf-8 characters:

        int b;
        StringBuilder buf = new StringBuilder(512);
        while ((b = br.read()) != -1) {
            buf.append((char) b);
        }

        br.close();
        isr.close();
        return buf.toString();
    }

    private void createPDF(Invoice invoice, OutputStream stream) throws Exception {
        DocumentController controller = new DocumentController(new PDDocument(), invoice, documentColumns);
        PDFFactory.createPDF(controller, stream, FONT_PATH, invoice);
    }

    private void initPDF() {
        List<Object[]> columns = new ArrayList<Object[]>();
        columns.add(new Object[]{"Lp.", 20f});
        columns.add(new Object[]{"Nazwa towaru lub usługi", 220f});
        columns.add(new Object[]{"Symbol\nPKWiU", 40f});
        columns.add(new Object[]{" Jm.", 30f});
        columns.add(new Object[]{"Ilość", 30f});
        columns.add(new Object[]{"Cena\nnetto", 50f});
        columns.add(new Object[]{"Wartość\nnetto", 55f});
        columns.add(new Object[]{"VAT %", 37f});
        columns.add(new Object[]{"Kwota\nVAT", 55f});
        columns.add(new Object[]{"Wartość\nbrutto", 55f});

        documentColumns = new DocumentColumns(columns, 6);

    }
}
