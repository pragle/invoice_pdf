package pl.vane.invoice.utils;

import pl.vane.invoice.model.InvoiceDate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: michal@vane.pl
 * Date: 01.03.15
 * Time: 22:43
 * To change this template use File | Settings | File Templates.
 */
public class StringUtil {
    public static String bigDecimalString(BigDecimal value, int precision) {
        return value.setScale(precision, RoundingMode.HALF_UP).toString();
    }

    public static String formatBankAccount(BigDecimal value) {
        String mask = value.toString();
        String out = "";
        for(int i = 0;i<mask.length();i++) {
            if((i-2)%4 == 0) {
                out += " ";
            }
            out += mask.charAt(i);
        }
        return out;
    }

    private static final SimpleDateFormat invoiceNumberFormatter = new SimpleDateFormat("MM/yyyy", Locale.US);
    public static String getInvoiceNumber(InvoiceDate date) {
        String out = String.valueOf(date.getNum() > 9 ? date.getNum() : "0" + date.getNum());
        out+= "/"+invoiceNumberFormatter.format(date.getEnd_time());
        return out;
    }
    private static final SimpleDateFormat invoiceFileNameFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
    public static String getInvoiceFileName(InvoiceDate date) {
        String out = String.valueOf(date.getNum() > 9 ? date.getNum() : "0" + date.getNum());
        out+= "."+invoiceFileNameFormatter.format(date.getMake_time());
        return out;
    }
}
