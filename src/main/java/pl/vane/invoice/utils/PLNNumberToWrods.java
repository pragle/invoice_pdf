package pl.vane.invoice.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * Created with IntelliJ IDEA.
 * User: michal@vane.pl
 * Date: 01.03.15
 * Time: 21:52
 * To change this template use File | Settings | File Templates.
 */
// http://stackoverflow.com/questions/3911966/how-to-convert-number-to-words-in-java
public class PLNNumberToWrods {
    private static final String[] tensNames = {
            "",
            " dziesięć",
            " dwadzieścia",
            " trzydzieści",
            " czterdzieści",
            " pięćdziesiąt",
            " sześćdziesiąt",
            " siedemdziesiąt",
            " osiemdziesiąt",
            " dziewięćdziesiąt"
    };

    private static final String[] numNames = {
            "",
            " jeden",
            " dwa",
            " trzy",
            " cztery",
            " pięć",
            " sześć",
            " siedem",
            " osiem",
            " dziewięć",
            " dziesięć",
            " jedenaście",
            " dwanaście",
            " trzynaście",
            " czternaście",
            " piętnaście",
            " szesnaście",
            " siedemnaście",
            " osiemnaście",
            " dziewiętnaście"
    };

    private static String convertLessThanOneThousand(int number) {
        String soFar;

        if (number % 100 < 20){
            soFar = numNames[number % 100];
            number /= 100;
        }
        else {
            soFar = numNames[number % 10];
            number /= 10;

            soFar = tensNames[number % 10] + soFar;
            number /= 10;
        }
        if (number == 0) return soFar;
        else if(number == 1) return "sto "+soFar;
        else if(number == 2) return "dwieście "+soFar;
        else if(number == 3 || number == 4) return numNames[number]+"sta "+soFar;
        return numNames[number] + "set" + soFar;
    }


    public static String convert(BigDecimal number) {
        // 0 to 999 999 999 999
        if (number == BigDecimal.ZERO) { return "zero"; }

        String snumber = number.setScale(0, RoundingMode.FLOOR).toString();

        // pad with "0"
        String mask = "000000000000";
        DecimalFormat df = new DecimalFormat(mask);
        snumber = df.format(new BigDecimal(snumber));

        // XXXnnnnnnnnn
        int billions = Integer.parseInt(snumber.substring(0,3));
        // nnnXXXnnnnnn
        int millions  = Integer.parseInt(snumber.substring(3,6));
        // nnnnnnXXXnnn
        int hundredThousands = Integer.parseInt(snumber.substring(6,9));
        // nnnnnnnnnXXX
        int thousands = Integer.parseInt(snumber.substring(9,12));
        int check = 0;
        String tradBillions;
        int tb = Integer.parseInt(snumber.substring(2,3)) != 0 ? Integer.parseInt(snumber.substring(2,3)) : Integer.parseInt(snumber.substring(0,3));
        switch (tb) {
            case 0:
                tradBillions = "";
                break;
            case 1 :
                tradBillions = convertLessThanOneThousand(billions)
                        + " miliard ";
                break;
            default :
                tradBillions = convertLessThanOneThousand(billions)
                        + " miliardy ";
        }
        String result =  tradBillions;

        String tradMillions;
        int tm = Integer.parseInt(snumber.substring(3,6)) != 0 ? Integer.parseInt(snumber.substring(3,6)) : Integer.parseInt(snumber.substring(3,6));
        check = tm;
        if(check > 20 && check % 10 != 0) {
            check = 4;
        }
        switch (check) {
            case 0:
                tradMillions = "";
                break;
            case 1 :
                tradMillions = convertLessThanOneThousand(millions) + " milion ";
                break;
            case 2 :
            case 3 :
            case 4 :
                tradMillions = convertLessThanOneThousand(millions) + " miliony ";
                break;
            default :
                tradMillions = convertLessThanOneThousand(millions) + " milionów ";
        }
        result =  result + tradMillions;

        String tradHundredThousands;
        int tht = Integer.parseInt(snumber.substring(6,9)) != 0 ? Integer.parseInt(snumber.substring(6,9)) : Integer.parseInt(snumber.substring(6,9));
        check = tht;
        if(check > 20 && check % 10 != 0) {
            check = 4;
        }
        // 23, 24, tysięcy
        int thtTest = tht - (tht / 10 * 10);
        if(thtTest == 1 || thtTest > 4) {
            check = 5;
        }
        switch (check) {
            case 0:
                tradHundredThousands = "";
                break;
            case 1 :
                tradHundredThousands = " jeden tysiąc ";
                break;
            case 2 :
            case 3 :
            case 4 :
                tradHundredThousands = convertLessThanOneThousand(hundredThousands)+" tysiące ";
                break;
            default :
                tradHundredThousands = convertLessThanOneThousand(hundredThousands)+" tysięcy ";
                //tradHundredThousands = convertLessThanOneThousand(hundredThousands)
                        //+ " tysięcy ";
        }
        result =  result + tradHundredThousands;

        String tradThousand;
        tradThousand = convertLessThanOneThousand(thousands);
        result =  result + tradThousand;

        // remove extra spaces!
        return result.replaceAll("^\\s+", "").replaceAll("\\b\\s{2,}\\b", " ");
    }

    /**
     * testing
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(0)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(1)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(16)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(100)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(118)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(200)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(219)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(800)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(801)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(1316)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(13316)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(23316)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(30316)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(122316)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(120316)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(220316)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(223316)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(1000000)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(2000000)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(3000200)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(700000)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(9000000)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(9001000)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(123456789)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(2147483647)));
        System.out.println("*** " + PLNNumberToWrods.convert(BigDecimal.valueOf(3000000010L)));
        BigDecimal test = new BigDecimal("100.23");
        test = test.remainder(BigDecimal.ONE);
        test = test.setScale(2, RoundingMode.HALF_UP).movePointRight(2);
        System.out.println(test.setScale(0).toString()+"/100");
    }
}
