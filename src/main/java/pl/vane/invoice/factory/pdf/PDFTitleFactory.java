package pl.vane.invoice.factory.pdf;

import org.apache.pdfbox.pdmodel.font.PDFont;
import pl.vane.invoice.Constraint;
import pl.vane.invoice.controller.DocumentController;
import pl.vane.invoice.model.Invoice;
import pl.vane.invoice.model.InvoiceDate;
import pl.vane.invoice.model.dimension.PageSize;
import pl.vane.invoice.utils.StringUtil;
import pl.vane.pdf.PDFHelper;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: michal@vane.pl
 * Date: 01.03.15
 * Time: 19:33
 * To change this template use File | Settings | File Templates.
 */
public class PDFTitleFactory {
    private static final SimpleDateFormat invoiceDateFormatter = new SimpleDateFormat("dd.MM.yyyy", Locale.US);
    public static void createSubtitle(DocumentController controller, PDFont font, int fontSize) throws Exception{
        Invoice invoice = controller.getInvoice();

        String created1 = "Wystawiono w dniu : ";
        String created2 = invoiceDateFormatter.format(invoice.getDate().getMake_time());
        created2 += ", "+invoice.getCity();
        String endTime1 = "Data zakończenia dostawy/usługi : ";
        String endTime2 = invoiceDateFormatter.format(invoice.getDate().getEnd_time());
        String payTime1 = "Termin płatności : ";
        String payTime2 = invoiceDateFormatter.format(invoice.getDate().getPay_time());
        payTime2 += " ( "+invoice.getPay_time()+" "+invoice.getPay_jm()+" ) ";

        float width = PDFHelper.getTextWidth(font, fontSize, endTime1);
        width+=5;
        controller.decrementRowY();
        PDFHelper.drawString(controller.getCurrentStream(), font, fontSize, Constraint.LEADING,
                controller.getPositionX(), controller.getPositionY(), Arrays.asList(created1, endTime1, payTime1));
        controller.setPositionX(controller.getPositionX() + width);
        PDFHelper.drawString(controller.getCurrentStream(), font, fontSize, Constraint.LEADING,
                controller.getPositionX(), controller.getPositionY(), Arrays.asList(created2, endTime2, payTime2));
    }


    public static void createTitle(DocumentController controller, PDFont font, int fontSize) throws Exception {
        Invoice invoice = controller.getInvoice();
        PageSize pageSize = controller.getPageSize();

        String name = "Faktura "+invoice.getDate().getType()+" nr: "+ StringUtil.getInvoiceNumber(invoice.getDate());
        float width = PDFHelper.getTextWidth(font, fontSize, name);

        float out = pageSize.getWidth()-width-pageSize.getPadding().getRight();
        controller.setPositionX(out);
        controller.setPositionY(controller.getStartY());
        PDFHelper.drawString(controller.getCurrentStream(), font, fontSize, 0, out, controller.getPositionY(), Arrays.asList(name));

    }
}
