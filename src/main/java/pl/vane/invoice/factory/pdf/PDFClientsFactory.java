package pl.vane.invoice.factory.pdf;

import org.apache.pdfbox.pdmodel.font.PDFont;
import pl.vane.invoice.Constraint;
import pl.vane.invoice.controller.DocumentController;
import pl.vane.invoice.model.Client;
import pl.vane.invoice.model.Invoice;
import pl.vane.invoice.model.dimension.PageSize;
import pl.vane.pdf.PDFHelper;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: michal@vane.pl
 * Date: 01.03.15
 * Time: 19:34
 * To change this template use File | Settings | File Templates.
 */
public class PDFClientsFactory {
    public static void createBuyer(DocumentController controller, PDFont font, int fontSize, float y1, float y2) throws Exception {
        Invoice invoice = controller.getInvoice();
        PageSize pageSize = controller.getPageSize();

        float width = 280;
        float x2 = pageSize.getWidth()-width-pageSize.getPadding().getRight();
        float height = controller.getRowHeight();

        PDFHelper.drawRectangle(controller.getCurrentStream(), Color.BLACK, x2, y1, width, height, true, Color.WHITE);
        PDFHelper.drawRectangle(controller.getCurrentStream(), Color.BLACK, x2, y2, width, height*4);


        String buyerStr = "Nabywca";
        float tw = PDFHelper.getTextWidth(font, fontSize, buyerStr);
        float x = (float) ((width-tw)*0.5);
        PDFHelper.drawString(controller.getCurrentStream(), font, fontSize, 0,
                x2+x, y1+5, Arrays.asList(buyerStr));

        Client buyer = invoice.getBuyer();
        PDFHelper.drawString(controller.getCurrentStream(), font, fontSize, Constraint.LEADING,
                x2+10, y1-15, buyer.getData());
    }

    public static void createSeller(DocumentController controller, PDFont font, int fontSize, float y1, float y2) throws Exception {
        Invoice invoice = controller.getInvoice();
        PageSize pageSize = controller.getPageSize();

        float width = 280;
        float x1 = pageSize.getPadding().getLeft();

        PDFHelper.drawRectangle(controller.getCurrentStream(), Color.BLACK, x1, y1, width, 20, true, Color.WHITE);

        PDFHelper.drawRectangle(controller.getCurrentStream(), Color.BLACK, x1, y2, width, 80);

        String sellerStr = "Sprzedawca";
        float tw = PDFHelper.getTextWidth(font, fontSize, sellerStr);
        float x = (float) ((width-tw)*0.5);
        PDFHelper.drawString(controller.getCurrentStream(), font, fontSize, 0,
                x+x1, y1+5, Arrays.asList(sellerStr));

        Client seller = invoice.getSeller();
        PDFHelper.drawString(controller.getCurrentStream(), font, fontSize, Constraint.LEADING,
                x1+10, y1-15, seller.getData());
    }
}
