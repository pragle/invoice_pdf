package pl.vane.invoice.factory.pdf;

import org.apache.pdfbox.pdmodel.font.PDFont;
import pl.vane.invoice.Constraint;
import pl.vane.invoice.controller.DocumentController;
import pl.vane.invoice.model.Commodity;
import pl.vane.invoice.model.Invoice;
import pl.vane.invoice.model.dimension.PageSize;
import pl.vane.invoice.model.pdf.CommodityRow;
import pl.vane.pdf.PDFHelper;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: michal@vane.pl
 * Date: 01.03.15
 * Time: 19:31
 * To change this template use File | Settings | File Templates.
 */
public class PDFTableFactory {

    public static void createRows(DocumentController controller, PDFont font, int fontSize) throws Exception {
        PageSize pageSize = controller.getPageSize();
        Invoice invoice = controller.getInvoice();
        float numPage = ((invoice.getCommodity().size()*20)+controller.getPositionY())/pageSize.getHeight();
        System.out.println(numPage);
        List<Commodity> commodities = invoice.getCommodity();

        for(int i =0;i<commodities.size();i++) {
            float nextX = pageSize.getPadding().getLeft();
            CommodityRow row = controller.getCommodityRowList().get(i);
            int y = 0;
            if(controller.decrementRowY(row.getSize())) {
                //controller.setPositionY(controller.getStartY() - pageSize.getPadding().getTop());
                createHeader(controller, font, 10);
            }
            //controller.decrementRowY();
            for(Object[] o : controller.getColumns().getColumns()) {

                String val = row.getRow()[y];
                String[] text = val.split("\n");
                float textPositionY = controller.getPositionY()+((row.getSize()-1)*controller.getRowHeight());

                PDFHelper.drawString(controller.getCurrentStream(), font, fontSize, Constraint.LEADING,
                        nextX+2, textPositionY, Arrays.asList(text));

                PDFHelper.drawRectangle(controller.getCurrentStream(), Color.BLACK,
                        nextX, controller.getPositionY(), (Float) (o[1]), row.getSize()*controller.getRowHeight());

                nextX += (Float)(o[1]);
                y+= 1;
            }
            //stream.addRect(padding.getLeft(), nextY, rowWidth, rowHeight);
            //System.out.println(nextY);
        }
    }



    public static void createHeader(DocumentController controller, PDFont font, int fontSize) throws Exception {
        PageSize pageSize = controller.getPageSize();
        float rowHeight = controller.getHeaderHeight();
        float nextX = pageSize.getPadding().getLeft();
        controller.getCurrentStream().setNonStrokingColor(Color.WHITE);
        for(Object[] o : controller.getColumns().getColumns()) {
            PDFHelper.drawRectangle(controller.getCurrentStream(), Color.BLACK, nextX,
                    controller.getPositionY(), (Float)(o[1]), rowHeight, true, Color.WHITE);

            String[] draw = ((String)(o[0])).split("\n");

            PDFHelper.drawString(controller.getCurrentStream(), font, fontSize, Constraint.LEADING,
                    nextX+2, controller.getPositionY()+25, Arrays.asList(draw));
            nextX += (Float)(o[1]);
        }
    }
}
