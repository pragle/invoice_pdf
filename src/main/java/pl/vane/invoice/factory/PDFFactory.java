package pl.vane.invoice.factory;

import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import pl.vane.invoice.Constraint;
import pl.vane.invoice.controller.DocumentController;
import pl.vane.invoice.factory.pdf.PDFClientsFactory;
import pl.vane.invoice.factory.pdf.PDFTableFactory;
import pl.vane.invoice.factory.pdf.PDFTitleFactory;
import pl.vane.invoice.model.Client;
import pl.vane.invoice.model.Invoice;
import pl.vane.invoice.model.dimension.Padding;
import pl.vane.invoice.model.pdf.DocumentColumns;
import pl.vane.invoice.utils.PLNNumberToWrods;
import pl.vane.invoice.utils.StringUtil;
import pl.vane.pdf.PDFHelper;

import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: michal@vane.pl
 * Date: 28.02.15
 * Time: 22:54
 * To change this template use File | Settings | File Templates.
 */
public class PDFFactory {

    static final int TO_PAY = 1;
    static final int PAID = 2;

    public static void createPDF(DocumentController controller, OutputStream stream, String fontPath, Invoice invoice) throws Exception{
        // Create a document and add a page to it
        controller.addPage();
        System.out.println(controller.getPageSize());

        File f = new File(fontPath);
        PDFont font = PDType0Font.load(controller.getDocument(), f);

        PDFTitleFactory.createTitle(controller, font, 24);
        PDFTitleFactory.createSubtitle(controller, font, 11);

        controller.decrementRowY(4);
        float y1 = controller.getPositionY();

        controller.decrementRowY(4);
        float y2 = controller.getPositionY();

        PDFClientsFactory.createSeller(controller, font, 11, y1, y2);
        PDFClientsFactory.createBuyer(controller, font, 11, y1, y2);

        controller.decrementRowY(3);
        PDFTableFactory.createHeader(controller, font, 10);
        PDFTableFactory.createRows(controller, font, 10);

        Object[] included = controller.getIncluded();
        if(controller.checkSection(included.length+1)) {
            controller.addPage();
            PDFTableFactory.createHeader(controller, font, 10);
        }
        createSummaryTable(controller, font, 10, included);

        if(controller.checkSection(5)) {
            controller.addPage();
        }
        controller.decrementRowY(2);
        if(invoice.getType() == TO_PAY) {
            createSummaryToPay(controller, font, 11);
        } else if(invoice.getType() == PAID) {
            createSummaryPaid(controller, font, 11);
        } else {
            throw new RuntimeException("Invalid invoice type type : " + invoice.getType());
        }

        if(controller.checkSection(7)) {
            controller.addPage();
        }
        createNotes(controller, font, 11);

        if(controller.checkSection(5)) {
            controller.addPage();
        }
        controller.decrementRowY(4);
        createSignature(controller, font, 10, invoice);

        //ByteArrayOutputStream stream = new ByteArrayOutputStream();
        controller.saveDocument(stream);
        //return stream;
    }

    private static void createSignature(DocumentController controller, PDFont font, int fontSize, Invoice invoice) throws Exception {
        PDPageContentStream stream = controller.getCurrentStream();
        float x2 = controller.getPageSize().getPadding().getLeft();
        stream.moveTo(x2, controller.getPositionY());
        stream.lineTo(x2+250, controller.getPositionY());

        stream.moveTo(x2+342, controller.getPositionY());
        stream.lineTo(x2+592, controller.getPositionY());
        stream.stroke();

        PDFHelper.drawString(stream, font, fontSize, Constraint.LEADING,
                x2+392, controller.getPositionY()+2, Arrays.asList(invoice.getSignature()));

        controller.decrementRowY();
        PDFHelper.drawString(stream, font, fontSize, Constraint.LEADING,
                x2+controller.getRowHeight(), controller.getPositionY()+2,
                Arrays.asList("Osoba upoważniona do odbioru faktury"));

        PDFHelper.drawString(stream, font, fontSize, Constraint.LEADING,
                x2+362, controller.getPositionY()+2,
                Arrays.asList("Osoba upoważniona do wystawienia faktury"));
    }

    private static void createNotes(DocumentController controller, PDFont font, int fontSize) throws Exception {
        PDPageContentStream stream = controller.getCurrentStream();
        float x2 = 252f+controller.getPageSize().getPadding().getLeft();

        controller.decrementRowY(2);
        PDFHelper.drawString(stream, font, fontSize, Constraint.LEADING,
                x2, controller.getPositionY()+2, Arrays.asList(" Uwagi:"));
        controller.decrementRowY(5);
        PDFHelper.drawRectangle(stream, Color.BLACK,
                x2, controller.getPositionY(), 340, 120);
    }

    private static void createSummaryToPay(DocumentController controller, PDFont font, int fontSize) throws Exception {
        PDPageContentStream stream = controller.getCurrentStream();
        float x1 = 120f;
        float x2 = 252f+controller.getPageSize().getPadding().getLeft();

        String[] summary = controller.getSummary();
        PDFHelper.drawString(stream, font, fontSize, Constraint.LEADING,
                x1, controller.getPositionY()+2, Arrays.asList("RAZEM DO ZAPŁATY : "));
        PDFHelper.drawString(stream, font, fontSize, Constraint.LEADING,
                x2, controller.getPositionY()+2, Arrays.asList(summary[3]+" zł"));

        controller.decrementRowY();
        PDFHelper.drawString(stream, font, fontSize, Constraint.LEADING,
                x1, controller.getPositionY() + 2, Arrays.asList("Słownie złotych : "));
        BigDecimal bd = new BigDecimal(summary[3]);
        String str = PLNNumberToWrods.convert(bd);
        bd = bd.remainder(BigDecimal.ONE).setScale(2, RoundingMode.HALF_UP).movePointRight(2);
        str += " "+StringUtil.bigDecimalString(bd, 0)+"/100";
        PDFHelper.drawString(stream, font, fontSize, Constraint.LEADING,
                x2, controller.getPositionY()+2, Arrays.asList(str));

        controller.decrementRowY();
        PDFHelper.drawString(stream, font, fontSize, Constraint.LEADING,
                x2, controller.getPositionY()+2, Arrays.asList("Sposób zapłaty : Przelew"));

        controller.decrementRowY();
        Client seller = controller.getInvoice().getSeller();
        str = StringUtil.formatBankAccount(seller.getBank_account());
        PDFHelper.drawString(stream, font, fontSize, Constraint.LEADING,
                x2, controller.getPositionY() + 2, Arrays.asList("Na konto : " + str));
    }

    private static void createSummaryPaid(DocumentController controller, PDFont font, int fontSize) throws Exception {
        PDPageContentStream stream = controller.getCurrentStream();
        float x1 = 120f;
        float x2 = 252f+controller.getPageSize().getPadding().getLeft();

        String[] summary = controller.getSummary();
        PDFHelper.drawString(stream, font, fontSize, Constraint.LEADING,
                x1, controller.getPositionY()+2, Arrays.asList("ZAPŁACONO : "));
        PDFHelper.drawString(stream, font, fontSize, Constraint.LEADING,
                x2, controller.getPositionY()+2, Arrays.asList(summary[3]+" zł"));

        controller.decrementRowY();
        PDFHelper.drawString(stream, font, fontSize, Constraint.LEADING,
                x1, controller.getPositionY() + 2, Arrays.asList("Słownie złotych : "));
        BigDecimal bd = new BigDecimal(summary[3]);
        String str = PLNNumberToWrods.convert(bd);
        bd = bd.remainder(BigDecimal.ONE).setScale(2, RoundingMode.HALF_UP).movePointRight(2);
        str += " "+StringUtil.bigDecimalString(bd, 0)+"/100";
        PDFHelper.drawString(stream, font, fontSize, Constraint.LEADING,
                x2, controller.getPositionY()+2, Arrays.asList(str));

        controller.decrementRowY();
        PDFHelper.drawString(stream, font, fontSize, Constraint.LEADING,
                x2, controller.getPositionY()+2, Arrays.asList("Sposób zapłaty : Gotówką"));
    }

    private static void createSummaryTable(DocumentController controller,
                                           PDFont font, int fontSize, Object[] included) throws Exception{
        Padding padding = controller.getPageSize().getPadding();
        DocumentColumns columns = controller.getColumns();
        PDPageContentStream stream = controller.getCurrentStream();

        controller.decrementRowY();

        int startX = columns.getSummaryColumn();
        float nextX = columns.getSummaryX()+padding.getLeft();

        int size = columns.getColumns().size();
        float xx = nextX-(Float)(columns.getColumns().get(startX-1)[1]);
        PDFHelper.drawString(stream, font, fontSize, Constraint.LEADING,
                xx+2, controller.getPositionY()+2, Arrays.asList("RAZEM : "));

        int j = 0;
        String[] summary = controller.getSummary();
        for(int i = startX;i<size;i++) {
            Object[] o = columns.getColumns().get(i);
            PDFHelper.drawRectangle(stream, Color.BLACK,
                    nextX, controller.getPositionY(), (Float) (o[1]), controller.getRowHeight());
            PDFHelper.drawString(stream, font, fontSize, Constraint.LEADING,
                    nextX+2, controller.getPositionY()+2, Arrays.asList(summary[j]));
            nextX += (Float)(o[1]);
            j += 1;
        }

        for(int k =0;k<included.length;k++) {
            nextX = columns.getSummaryX()+padding.getLeft();
            controller.decrementRowY();
            PDFHelper.drawString(stream, font, fontSize, Constraint.LEADING,
                    xx+2, controller.getPositionY()+2, Arrays.asList("W tym : "));
            j = 0;
            String[] in = (String[]) included[k];
            for(int i = startX;i<size;i++) {
                Object[] o = columns.getColumns().get(i);
                PDFHelper.drawRectangle(stream, Color.BLACK,
                        nextX, controller.getPositionY(), (Float) (o[1]), controller.getRowHeight());
                PDFHelper.drawString(stream, font, fontSize, Constraint.LEADING,
                        nextX+2, controller.getPositionY()+2, Arrays.asList(in[j]));
                nextX += (Float)(o[1]);
                j += 1;
            }
        }
    }
}
