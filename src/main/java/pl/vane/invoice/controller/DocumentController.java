package pl.vane.invoice.controller;

import lombok.Getter;
import lombok.Setter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import pl.vane.invoice.Constraint;
import pl.vane.invoice.model.Commodity;
import pl.vane.invoice.model.Invoice;
import pl.vane.invoice.model.dimension.Padding;
import pl.vane.invoice.model.dimension.PageSize;
import pl.vane.invoice.model.pdf.CommodityRow;
import pl.vane.invoice.model.pdf.DocumentColumns;
import pl.vane.invoice.utils.StringUtil;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.*;


/**
 * Created with IntelliJ IDEA.
 * User: michal@vane.pl
 * Date: 01.03.15
 * Time: 18:33
 * To change this template use File | Settings | File Templates.
 */
public class DocumentController {

    @Getter private final PDDocument document;
    @Getter private PDPage currentPage;
    @Getter private PDPageContentStream currentStream;
    @Getter private PageSize pageSize;

    @Getter private final float headerHeight;
    @Getter private final float rowHeight;

    @Getter @Setter private float positionX;
    @Getter @Setter private float positionY;
    @Getter float startX;
    @Getter float startY;

    @Getter float textPadding;

    @Getter private Invoice invoice;
    @Getter private DocumentColumns columns;

    @Getter private List<CommodityRow> commodityRowList;

    private Map<BigDecimal, BigDecimal> nettoMap = new HashMap<BigDecimal, BigDecimal>();
    private Map<BigDecimal, BigDecimal> bruttoMap = new HashMap<BigDecimal, BigDecimal>();
    private Map<BigDecimal, BigDecimal> vatMap = new HashMap<BigDecimal, BigDecimal>();

    public DocumentController(PDDocument document, Invoice invoice, DocumentColumns columns) {
        this.document = document;
        this.invoice = invoice;
        this.columns = columns;
        this.startY = 740;
        this.rowHeight = 20;
        this.headerHeight = 40;
        this.textPadding = 5;
        this.commodityRowList = new ArrayList<CommodityRow>();
        calculate();
    }

    private void calculate() {
        int x = 1;
        for(Commodity commodity : invoice.getCommodity()) {
            CommodityRow row = createRowText(x, columns.getColumns().size(), commodity);
            if(!nettoMap.containsKey(row.getVatType()))  {
                nettoMap.put(row.getVatType(), BigDecimal.ZERO);
                bruttoMap.put(row.getVatType(), BigDecimal.ZERO);
                vatMap.put(row.getVatType(), BigDecimal.ZERO);
            }
            BigDecimal netto = nettoMap.get(row.getVatType()).add(row.getNetto());
            nettoMap.put(row.getVatType(), netto);

            BigDecimal vat = vatMap.get(row.getVatType()).add(row.getVat());
            vatMap.put(row.getVatType(), vat);

            BigDecimal brutto = bruttoMap.get(row.getVatType()).add(row.getBrutto());
            bruttoMap.put(row.getVatType(), brutto);

            commodityRowList.add(row);
            x++;
        }
    }

    public void addPage() throws IOException {
        if(currentPage != null) {
            currentStream.close();
        }
        PDPage page = new PDPage();
        document.addPage( page );
        currentPage = page;
        currentStream = new PDPageContentStream(document, page);
        Padding padding = new Padding(0, 10, 0, 10);
        this.pageSize = new PageSize(page.getMediaBox().getWidth(), page.getMediaBox().getHeight(), padding);
        // set start positionY
        positionY = startY - pageSize.getPadding().getTop();
    }

    public boolean decrementRowY(int num) throws IOException {
        float val = rowHeight*num;
        if(positionY-val < 0) {
            addPage();
            return true;
        }
        positionY -= val;
        return false;
    }

    public boolean decrementRowY() throws IOException {
        return decrementRowY(1);
    }

    public boolean checkSection(int num) {
        if(positionY-(num * rowHeight) < 0) {
            return true;
        }
        return false;
    }

    public void saveDocument(OutputStream stream) throws IOException {
        if(currentPage != null) {
            currentStream.close();
        }
        document.save(stream);
        document.close();
    }

    public Object[] getIncluded() {
        Set<BigDecimal> keys = bruttoMap.keySet();
        Map<BigDecimal, String[]> out = new HashMap<BigDecimal, String[]>();
        for(BigDecimal key : keys) {
            String[] str = new String[4];
            str[0] = StringUtil.bigDecimalString(nettoMap.get(key), 2);
            str[1] = StringUtil.bigDecimalString(key, 0)+"%";
            str[2] = StringUtil.bigDecimalString(vatMap.get(key), 2);
            str[3] = StringUtil.bigDecimalString(bruttoMap.get(key), 2);
            out.put(key, str);
        }
        return out.values().toArray();
    }

    public String[] getSummary() {
        String[] out = new String[4];
        BigDecimal netto = BigDecimal.ZERO;
        BigDecimal vat = BigDecimal.ZERO;
        BigDecimal brutto = BigDecimal.ZERO;
        for(BigDecimal val : nettoMap.values()) {
            netto = netto.add(val);
        }
        for(BigDecimal val : vatMap.values()) {
            vat = vat.add(val);
        }
        for(BigDecimal val : bruttoMap.values()) {
            brutto = brutto.add(val);
        }
        out[0] = StringUtil.bigDecimalString(netto, 2);
        out[1] = "";
        out[2] = StringUtil.bigDecimalString(vat, 2);
        out[3] = StringUtil.bigDecimalString(brutto, 2);
        return out;
    }

    private CommodityRow createRowText(int index, int size, Commodity commodity) {
        String[] out = new String[size];
        int textSize = commodity.getName().split("\n").length;
        out[0] = index+".";
        out[1] = commodity.getName();
        out[2] = commodity.getPkwiu();
        out[3] = commodity.getJm();
        out[4] = ""+commodity.getQuantity();
        out[5] = StringUtil.bigDecimalString(commodity.getNetto(), 2);
        BigDecimal netto = commodity.getNetto().multiply(commodity.getQuantity());
        out[6] = StringUtil.bigDecimalString(netto, 2);
        out[7] = StringUtil.bigDecimalString(commodity.getVat(), 0)+"%";
        BigDecimal vat = netto.multiply(commodity.getVat()).divide(Constraint.HUNDRED);
        out[8] = StringUtil.bigDecimalString(vat, 2);
        BigDecimal brutto = vat.add(netto);
        out[9] = StringUtil.bigDecimalString(brutto, 2);
        return new CommodityRow(commodity, vat, netto, brutto, commodity.getVat(), out, textSize);
    }
}
