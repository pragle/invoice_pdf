package pl.vane.invoice.model.pdf;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.vane.invoice.model.Commodity;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: michal@vane.pl
 * Date: 01.03.15
 * Time: 17:26
 * To change this template use File | Settings | File Templates.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommodityRow {
    private Commodity commodity;
    private BigDecimal vat;
    private BigDecimal netto;
    private BigDecimal brutto;
    private BigDecimal vatType;
    private String[] row;
    private int size;
}
