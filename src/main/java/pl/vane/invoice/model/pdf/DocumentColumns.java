package pl.vane.invoice.model.pdf;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created with IntelliJ IDEA.
 * User: michal@vane.pl
 * Date: 01.03.15
 * Time: 19:29
 * To change this template use File | Settings | File Templates.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DocumentColumns {
    private java.util.List<Object[]> columns;
    private int summaryColumn;

    public float getTableWidth() {
        float out = 0f;
        for(Object[] c : columns) {
            out += (Float)(c[1]);
        }
        return out;
    }

    public float getSummaryX() {
        float out = 0f;
        int x = 0;
        do {
            out += (Float)(columns.get(x)[1]);
            x += 1;
        } while (x < summaryColumn);
        return out;
    }
}
