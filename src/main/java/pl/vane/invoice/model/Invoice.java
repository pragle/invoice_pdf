package pl.vane.invoice.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: michal@vane.pl
 * Date: 28.02.15
 * Time: 22:23
 * To change this template use File | Settings | File Templates.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "json")
public class Invoice implements Serializable {
    private InvoiceDate date;
    private String signature;
    private int type;
    private Client seller;
    private Client buyer;
    private int pay_time;
    private String pay_jm;
    private String name;
    private String city;
    private List<Commodity> commodity;
}
