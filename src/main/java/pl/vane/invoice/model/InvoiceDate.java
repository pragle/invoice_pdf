package pl.vane.invoice.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: michal@vane.pl
 * Date: 28.02.15
 * Time: 22:27
 * To change this template use File | Settings | File Templates.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "json")
public class InvoiceDate implements Serializable {
    private Date make_time;
    private Date end_time;
    private Date pay_time;
    private int num;
    private String type;
}
