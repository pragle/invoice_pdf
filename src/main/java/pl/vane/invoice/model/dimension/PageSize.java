package pl.vane.invoice.model.dimension;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created with IntelliJ IDEA.
 * User: michal@vane.pl
 * Date: 01.03.15
 * Time: 18:27
 * To change this template use File | Settings | File Templates.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageSize {
    private float width;
    private float height;
    private Padding padding;
}
