package pl.vane.invoice;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: michal@vane.pl
 * Date: 01.03.15
 * Time: 19:31
 * To change this template use File | Settings | File Templates.
 */
public class Constraint {
    public static BigDecimal HUNDRED = new BigDecimal("100");
    public static double LEADING = 12 * 1.2;
}
