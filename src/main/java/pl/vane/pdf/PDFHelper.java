package pl.vane.pdf;

import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: michal@vane.pl
 * Date: 01.03.15
 * Time: 02:09
 * To change this template use File | Settings | File Templates.
 */
public class PDFHelper {

    public static void drawString(PDPageContentStream stream, PDFont font, int fontSize,
                                  double leading, float x, float y, List<String> text) throws Exception {
        stream.setStrokingColor(Color.BLACK);
        stream.setNonStrokingColor(Color.BLACK);
        stream.beginText();
        stream.setFont(font, fontSize);
        stream.setLeading(leading);
        stream.newLineAtOffset(x, y);
        for(String line : text) {
            stream.showText(line);
            stream.newLine();
        }
        stream.endText();

    }

    public static float getTextWidth(PDFont font, int fontSize, String txt) throws Exception{
        return font.getStringWidth(txt)/1000*fontSize;
    }

    public static void drawRectangle(PDPageContentStream stream, Color color, float x, float y, float width, float height) throws Exception{
        drawRectangle(stream, color, x, y, width, height, false, null);
    }

    public static void drawRectangle(PDPageContentStream stream, Color color, float x, float y, float width, float height, boolean fill, Color fillColor) throws Exception{
        if(fill) {
            stream.addRect(x, y, width, height);
            stream.setNonStrokingColor(fillColor);
            stream.fill();
        }
        stream.setStrokingColor(color);
        stream.addRect(x, y, width, height);
        stream.stroke();
    }

}
